import React, {Component} from 'react';
import 'bootstrap/dist/css/bootstrap.css'
import ReactDom from 'react-dom'

class Timer extends Component {

    state = {
        second: 0,
        minute: 0,
        hour: 0,
        isDisabled: false,
        interval: '',
        savedIntervals: [],
        isCompleted: false
    }

    increaseHour = () => {
        let {hour} = this.state
        this.setState({
            hour: hour + 1
        })
    }

    decreaseHour = () => {
        let {hour} = this.state
        this.setState({
            hour: (hour > 0) ? hour - 1 : hour,
        })
    }

    increaseMinute = () => {
        let {minute} = this.state
        this.setState({
            minute: (minute < 59) ? minute + 1 : minute
        })
    }

    decreaseMinute = () => {
        let {minute} = this.state
        this.setState({
            minute: (minute > 0) ? minute - 1 : minute
        })
    }

    increaseSecond = () => {
        let {second} = this.state
        this.setState({
            second: (second < 59) ? second + 1 : second
        })
    }

    decreaseSecond = () => {
        let {second} = this.state
        this.setState({
            second: (second > 0) ? second - 1 : second
        })
    }

    onStartClicked = () => {

        this.setState({
            isDisabled: true
        })
        let i = setInterval(() => {
            const {second, minute, hour} = this.state

            if (second === 1 && minute === 0 && hour === 0) {
                this.setState({
                    isCompleted: true,
                    hour: 0,
                    minute: 0,
                    second: 0
                })

                clearInterval(this.state.interval)
            }

            if (second === 0) {
                this.setState({
                    second: 59,
                    minute: minute - 1
                })

                if (minute === 0) {
                    this.setState({
                        minute: 59,
                        hour: hour - 1
                    })
                }

            } else {
                this.setState({
                    second: second - 1
                })
            }
        }, 1000)

        this.setState({
            interval: i
        })
    }

    onStopClicked = () => {
        clearInterval(this.state.interval)
        this.setState({
            isDisabled: false
        })
    }

    onIntervalClicked = () => {
        let {savedIntervals, hour, minute, second} = this.state
        savedIntervals.push(hour + " : " + minute + " : " + second)
        this.setState({
            savedIntervals: savedIntervals
        })

    }

    onResetClicked = () => {
        this.onStopClicked();

        this.setState({
            hour: 0,
            minute: 0,
            second: 0,
            savedIntervals: []
        })
    }

    render() {

        const {hour, minute, second, isDisabled, savedIntervals, isCompleted} = this.state
        return (
            <div>

                <div className="container mt-4 border p-5">
                    <div className="row offset-1">
                        <div className="col-md-4 mt-5">
                            <div className="card">
                                <ul className="list-group list-group-flush">
                                    <li className="list-group-item d-flex justify-content-around">
                                        <h6>Hour : </h6>
                                        <button onClick={this.increaseHour} className={"btn btn-success"}>+</button>
                                        <b>{this.state.hour}</b>
                                        <button onClick={this.decreaseHour} className={'btn btn-danger'}>-</button>
                                    </li>
                                    <li className="list-group-item d-flex justify-content-around">
                                        <h6>Minute : </h6>
                                        <button onClick={this.increaseMinute} className={"btn btn-success"}>+</button>
                                        <b>{this.state.minute}</b>
                                        <button onClick={this.decreaseMinute} className={'btn btn-danger'}>-</button>
                                    </li>
                                    <li className="list-group-item d-flex justify-content-around">
                                        <h6>Second : </h6>
                                        <button onClick={this.increaseSecond} className={"btn btn-success"}>+</button>
                                        <b>{this.state.second}</b>
                                        <button onClick={this.decreaseSecond} className={'btn btn-danger'}>-</button>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div className="col-md-6">
                            <div className="card">
                                <div className="card-header">
                                    <h1 className={'text-center'}>Timer</h1>
                                </div>
                                <div className="card-body">
                                    <h1 className={'text-center'}>{hour} : {minute} : {second}</h1>
                                </div>
                                <div className="card-footer">
                                    <div className="row">
                                        <div className="col-md-3">
                                            <button onClick={this.onStartClicked} className="btn btn-success"
                                                    disabled={isDisabled}>Start
                                            </button>
                                        </div>
                                        <div className="col-md-3">
                                            <button onClick={this.onStopClicked} className="btn btn-danger">Stop
                                            </button>
                                        </div>
                                        <div className="col-md-3">
                                            <button onClick={this.onIntervalClicked}
                                                    className="btn btn-warning" disabled={!isDisabled}>Interval
                                            </button>
                                        </div>
                                        <div className="col-md-3">
                                            <button onClick={this.onResetClicked} className="btn btn-primary">Reset
                                            </button>
                                        </div>
                                    </div>
                                </div>

                                {isCompleted ? <h3 className={'text-center text-success'}>Done it bro!</h3> :
                                    <h3 className={'text-center text-warning'}>You have to run!!!</h3>}

                                {savedIntervals.map((item, index) => <p className={'mx-5 border text-center'}
                                                                        key={index}>{item}</p>)}
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        );
    }
}

ReactDom.render(
    <Timer/>,
    document.getElementById('root')
)